# -*- makefile -*-
NAME = "PullDown Covers"
SUMMARY = "Changes the cover action buttons to pulldown menus"
DESCRIPTION := $(SUMMARY)
LICENSE=BSD
VER=0.8
MAINTAINER=Thaodan
# see https://github.com/sailfishos-patches/patchmanager#categories
CATEGORY=homescreen
